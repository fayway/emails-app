import { Email } from './email.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../users/user.entity';

@Injectable()
export class EmailsService {
  constructor(
    @InjectRepository(Email) private readonly emailRepository: Repository<Email>
  ) {}

  getEmailById(id: string): Promise<Email> {
    return this.emailRepository.findOne(id);
  }

  sendEmail(email: Email) {
    return this.emailRepository.save(email);
  }

  getEmailsSentByUser(user: User): Promise<Email[]> {
    return this.emailRepository.find({ where: { fromUser: user } });
  }

  getEmailsReceivedByUser(user: User): Promise<Email[]> {
    return this.emailRepository.find({
      where: { toEmail: user.email },
      relations: ['fromUser'],
    });
  }
}
