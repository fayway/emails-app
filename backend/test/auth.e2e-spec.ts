import { ValidationPipe, Dependencies } from '@nestjs/common';
import request from 'supertest';
import { Test } from '@nestjs/testing';
import { AppModule } from './../src/app.module';
import { INestApplication, Controller, Get, UseGuards } from '@nestjs/common';
import { AuthModule } from '../src/auth/auth.module';
import { jwtRegex } from './test.util';
import { AuthGuard } from '@nestjs/passport';
import { UsersService } from '../src/users/users.service';
import { usersToCreate } from './test.data';
import { BaseTestingModule } from './base-testing.module';
import { TestUtilsService } from './test.utils.service';

@Controller('jwt-protected')
export class TestController {
  // simple test to make sure that a route is protected and
  // that the user needs to provide a valid JWT token to access it
  @Get('data')
  @UseGuards(AuthGuard('jwt'))
  getJwtProtectedData() {
    return { data: { protected: true } };
  }
}

describe('AuthController (e2e)', () => {
  let app: INestApplication;
  let usersService: UsersService;
  let testUtilsService: TestUtilsService;

  beforeEach(async () => {
    // setup testing module
    const moduleFixture = await Test.createTestingModule({
      imports: [BaseTestingModule, AuthModule],
      controllers: [TestController],
    }).compile();

    // create the app
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    // get the dependencies
    usersService = moduleFixture.get(UsersService);
    testUtilsService = moduleFixture.get(TestUtilsService);

    // clean the database
    await testUtilsService.cleanAllDatabaseEntities();
  });

  afterEach(async () => {
    // close the database connection
    // this is important otherwise not the first test but others
    // won't be able to connect and will timeout without other error...
    await testUtilsService.closeDbConnection();
  });

  it('should get a new token for an existing user: [POST] /auth/token', async () => {
    await usersService.register(usersToCreate.user1);

    return request(app.getHttpServer())
      .post('/auth/token')
      .send({
        email: usersToCreate.user1.email,
        password: usersToCreate.user1.password,
      })
      .expect(201)
      .then(x => {
        expect(x.body.expiresIn).toBe(3600);
        expect(x.body.accessToken).toEqual(expect.stringMatching(jwtRegex));
      });
  });

  it('should get a bad request if user does not exist [POST] /auth/token', () => {
    return request(app.getHttpServer())
      .post('/auth/token')
      .send({
        email: 'some-random-user',
        password: 'pwd',
      })
      .expect({ statusCode: 400, message: 'Password and email do not match' });
  });

  it('should get a bad request if user/password does not match [POST] /auth/token', async () => {
    await usersService.register(usersToCreate.user1);

    return request(app.getHttpServer())
      .post('/auth/token')
      .send({
        email: usersToCreate.user1.email,
        password: 'random pwd',
      })
      .expect({ statusCode: 400, message: 'Password and email do not match' });
  });

  it('should not have access to a JWT protected route without JWT', () => {
    return request(app.getHttpServer())
      .get('/jwt-protected/data')
      .expect({ statusCode: 401, error: 'Unauthorized' });
  });

  it('should not have access to a JWT protected route with an invalid JWT token', () => {
    return request(app.getHttpServer())
      .get('/jwt-protected/data')
      .set('Authorization', 'bearer ' + 'random-jwt')
      .expect({ statusCode: 401, error: 'Unauthorized' });
  });

  it('should have access to a JWT protected route with a valid JWT token', async () => {
    await usersService.register(usersToCreate.user1);

    const agent = request(app.getHttpServer());

    const res = await agent
      .post('/auth/token')
      .send({
        email: usersToCreate.user1.email,
        password: usersToCreate.user1.password,
      })
      .expect(201);

    await agent
      .get('/jwt-protected/data')
      .set('Authorization', 'bearer ' + res.body.accessToken)
      .expect(200)
      .expect({
        data: { protected: true },
      });
  });
});
