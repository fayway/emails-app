import { RegisterUserDto } from '../src/users/user.entity';

interface UsersToCreate {
  [key: string]: RegisterUserDto;
}

export const usersToCreate: UsersToCreate = {
  user1: {
    id: '2c98ceef-63d7-4c28-8745-092efdad85b3',
    firstName: 'Bethany',
    lastName: 'Gardner',
    email: 'BethanyGGardner@teleworm.us',
    password: 'cuy3It2i',
  },
};
