import {
  Controller,
  Get,
  UseGuards,
  Post,
  Body,
  HttpException,
  HttpStatus,
  Req,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService, Token } from './auth.service';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './jwt-payload.interface';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UsersService
  ) {}

  @Post('token')
  async createToken(
    @Body('email') email: string,
    @Body('password') password: string
  ): Promise<Token> {
    const userFound = await this.userService.findMatchEmailPassword(email, password);

    // do not give details whether an email exists or if the password is wrong
    if (!userFound) {
      throw new HttpException(
        'Password and email do not match',
        HttpStatus.BAD_REQUEST
      );
    }

    return this.authService.createToken(email);
  }

  // simple test to make sure that a route is protected and
  // that the user needs to provide a valid JWT token to access it
  @Get('data')
  @UseGuards(AuthGuard('jwt'))
  findAll(@Req() req) {
    // possible to access current user with `req.user`
    return 'ok';
  }
}
