import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDraftsComponent } from './page-drafts.component';

describe('PageDraftsComponent', () => {
  let component: PageDraftsComponent;
  let fixture: ComponentFixture<PageDraftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PageDraftsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageDraftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
