import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { NewEmail } from '../../../interfaces/email.interface';

@Component({
  selector: 'app-new-email',
  templateUrl: './new-email.component.html',
  styleUrls: ['./new-email.component.scss'],
})
export class NewEmailComponent implements OnInit {
  newEmailForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<NewEmailComponent>,
  ) {}

  ngOnInit() {
    this.newEmailForm = this.fb.group({
      subject: ['', [Validators.required, Validators.minLength(3)]],
      toEmail: ['', [Validators.required, Validators.minLength(3)]],
      body: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

  sendEmail(): void {
    const { subject, toEmail, body } = this.newEmailForm.value;

    const newEmail: NewEmail = {
      subject,
      toEmail,
      body,
    };

    this.dialogRef.close(newEmail);
  }
}
