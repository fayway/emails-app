# Emails

## Description

CRUD for managing an email app (demo)

## Installation

```bash
$ yarn
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Start MySql for dev server
```
docker run --name mysql-emails-app -v /your/path/to/emails-app/backend/mysql:/var/lib/mysql -p 3312:3306 -e MYSQL_ROOT_PASSWORD=dev-pw -d mysql:5.7
```

To start PhpMyAdmin:

```
docker run --name phpmyadmin-emails-app --link mysql-emails-app:db -p 8082:80 -d phpmyadmin/phpmyadmin
```