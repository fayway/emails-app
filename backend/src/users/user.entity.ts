import { Entity, PrimaryColumn, Column } from 'typeorm';
import {
  IsString,
  IsInt,
  Length,
  IsUUID,
  IsEmail,
  IsNotEmpty,
} from 'class-validator';

@Entity()
export class User {
  @PrimaryColumn() id: string;

  @Column() firstName: string;

  @Column() lastName: string;

  @Column() email: string;

  @Column({ select: false })
  password: string;
}

export class RegisterUserDto {
  @IsUUID('4')
  @IsNotEmpty()
  id: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 25)
  firstName: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 25)
  lastName: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @Length(6, 100)
  password: string;
}
