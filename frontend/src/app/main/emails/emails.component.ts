import { Component, OnInit, Input } from '@angular/core';
import { Email } from '../../interfaces/email.interface';

@Component({
  selector: 'app-emails',
  templateUrl: './emails.component.html',
  styleUrls: ['./emails.component.scss'],
})
export class EmailsComponent implements OnInit {
  @Input() emails: Email[];

  constructor() {}

  ngOnInit() {}
}
