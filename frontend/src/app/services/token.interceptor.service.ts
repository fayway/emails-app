import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
} from '@angular/common/http';
import { Observable, EMPTY } from 'rxjs';
import { Token } from '../interfaces/token.interface';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root',
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private authService: AuthService,
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    if (
      !req.url.startsWith('http://localhost:3856/') ||
      req.url === 'http://localhost:3856/auth/token'
    ) {
      return next.handle(req);
    }

    const token: Token = this.authService.getToken();

    if (!token) {
      this.router
        .navigate(['/login'])
        .then(() => this.snackBar.open('Please log in before using the app'));

      return EMPTY;
    }

    const request: HttpRequest<any> = req.clone({
      setHeaders: {
        Authorization: `Bearer ${token.accessToken}`,
      },
    });

    return next.handle(request);
  }
}
