import { NewEmail } from './../../../interfaces/email.interface';
import { NewEmailComponent } from './../new-email/new-email.component';
import {
  catchError,
  tap,
  map,
  withLatestFrom,
  switchMap,
} from 'rxjs/operators';
import { EmailsService } from './../../../services/emails.service';
import { Component, OnInit } from '@angular/core';
import { Observable, EMPTY } from 'rxjs';
import { Email } from '../../../interfaces/email.interface';
import { MatSnackBar, MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-page-inbox',
  templateUrl: './page-inbox.component.html',
  styleUrls: ['./page-inbox.component.scss'],
})
export class PageInboxComponent implements OnInit {
  emails$: Observable<Email[]>;
  selectedEmail$: Observable<Email>;

  constructor(
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private emailsService: EmailsService,
  ) {}

  ngOnInit() {
    this.emails$ = this.emailsService.emails$;

    this.fetchEmails().subscribe();

    const selectedEmailId$ = this.route.queryParamMap.pipe(
      map(queryParams => queryParams.get('email')),
    );

    this.selectedEmail$ = selectedEmailId$.pipe(
      withLatestFrom(this.emails$),
      map(([emailId, emails]) => emails.find(email => email.id === emailId)),
    );
  }

  fetchEmails(): Observable<null> {
    return this.emailsService.fetchEmails().pipe(
      catchError(e => {
        this.snackBar.open(
          'An error occurred while retrieving the emails',
          undefined,
          { duration: 3000 },
        );
        return EMPTY;
      }),
    );
  }

  newEmail(): void {
    const dialogRef = this.dialog.open(NewEmailComponent, {
      width: '500px',
    });

    dialogRef
      .afterClosed()
      .pipe(
        switchMap((newEmail: NewEmail) =>
          this.emailsService.sendEmail({ ...newEmail, id: uuid() }),
        ),
        tap((email: Email) =>
          this.snackBar.open(
            `Email "${email.subject}" has been sent`,
            undefined,
            { duration: 5000 },
          ),
        ),
      )
      .subscribe();
  }
}
