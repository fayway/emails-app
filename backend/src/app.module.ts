import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DatabaseModule } from './database/database.module';
import { EmailsModule } from './emails/emails.module';

@Module({
  imports: [DatabaseModule, UsersModule, AuthModule, EmailsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
