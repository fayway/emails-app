import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { test } from '../ormconfig';
import { TestUtilsService } from './test.utils.service';
import { DatabaseService } from '../src/database/database.service';

@Module({
  imports: [TypeOrmModule.forRoot(test)],
  providers: [DatabaseService, TestUtilsService],
  exports: [DatabaseService, TestUtilsService],
})
export class BaseTestingModule {}
