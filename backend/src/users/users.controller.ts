import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  HttpStatus,
  HttpException,
  UsePipes,
} from '@nestjs/common';
import {
  UsersService,
  ErrorUserIdAlreadyExists,
  ErrorUserEmailAlreadyExists,
} from './users.service';
import { User, RegisterUserDto } from './user.entity';
import { checkSameIdUrlBody } from '../shared/helpers/helpers';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Put(':id')
  register(@Param('id') id: string, @Body() user: RegisterUserDto) {
    checkSameIdUrlBody(id, user);

    return this.usersService
      .register(user)
      .catch((e: Error) => {
        if (e instanceof ErrorUserIdAlreadyExists) {
          throw new HttpException(
            'This ID already exists, are you just trying to login?',
            HttpStatus.CONFLICT
          );
        } else if (e instanceof ErrorUserEmailAlreadyExists) {
          throw new HttpException(
            'This email already exists, are you just trying to login?',
            HttpStatus.CONFLICT
          );
        } else {
          throw e;
        }
      });
  }
}
