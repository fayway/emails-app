import { mapTo, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Email } from '../interfaces/email.interface';

@Injectable({
  providedIn: 'root',
})
export class EmailsService {
  private _emails$: Subject<Email[]> = new Subject();
  public emails$: Observable<Email[]> = this._emails$.asObservable();

  constructor(private httpClient: HttpClient) {}

  fetchEmails(): Observable<null> {
    return this.httpClient
      .get<Email[]>('http://localhost:3856/emails')
      .pipe(tap(emails => this._emails$.next(emails)), mapTo(null));
  }

  sendEmail(email: Email): Observable<Email> {
    return this.httpClient.post<Email>('http://localhost:3856/emails', email);
  }

  cleanUp() {
    this._emails$.next([]);
  }
}
