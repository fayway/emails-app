import { UsersService } from './../src/users/users.service';
import request from 'supertest';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { UsersModule } from '../src/users/users.module';
import { UsersController } from '../src/users/users.controller';
import { Test } from '@nestjs/testing';
import { usersToCreate } from './test.data';
import { RegisterUserDto } from '../src/users/user.entity';
import { BaseTestingModule } from './base-testing.module';
import { TestUtilsService } from './test.utils.service';
import { AuthModule } from '../src/auth/auth.module';

describe('Register (e2e)', () => {
  let app: INestApplication;
  let usersService: UsersService;
  let testUtilsService: TestUtilsService;

  beforeEach(async () => {
    // setup testing module
    const moduleFixture = await Test.createTestingModule({
      imports: [BaseTestingModule, UsersModule, AuthModule],
    }).compile();

    // create the app
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    // get the dependencies
    usersService = moduleFixture.get(UsersService);
    testUtilsService = moduleFixture.get(TestUtilsService);

    // clean the database
    await moduleFixture.get(TestUtilsService).cleanAllDatabaseEntities();
  });

  afterEach(async () => {
    // close the database connection
    // this is important otherwise not the first test but others
    // won't be able to connect and will timeout without other error...
    await testUtilsService.closeDbConnection();
  });

  // afterAll()

  it('should return 400 bad request if the ID in URL is different from the one in the object', () => {
    return request(app.getHttpServer())
      .put(`/users/8ec2c8e4-7dc4-424c-a043-2a2122c0b8f7`)
      .send(usersToCreate.user1)
      .expect({
        statusCode: 400,
        message: 'ID in the URL and the ID into the body must be the same',
      });
  });

  it('should return a 400 bad request if one of the parameters are missing', () => {
    const usersWithOnePropertyMissing: Partial<RegisterUserDto>[] = Object.keys(
      usersToCreate.user1
    ).map(key => ({ ...usersToCreate.user1, [key]: undefined }));

    return Promise.all(
      usersWithOnePropertyMissing.map(user =>
        request(app.getHttpServer())
          .put(`/users/${usersToCreate.user1.id}`)
          .send(user)
          .expect(({ body }) => {
            expect(body).toMatchObject({
              statusCode: 400,
              error: 'Bad Request',
            });
          })
      )
    );
  });

  it('should return 409 conflict if the ID is already taken', async () => {
    await usersService.register(usersToCreate.user1);

    return request(app.getHttpServer())
      .put(`/users/${usersToCreate.user1.id}`)
      .send({ ...usersToCreate.user1, email: 'randomEmail@g.com' })
      .expect({
        statusCode: 409,
        message: 'This ID already exists, are you just trying to login?',
      });
  });

  it('should return 409 conflict if the email is already taken', async () => {
    await usersService.register(usersToCreate.user1);

    return request(app.getHttpServer())
      .put(`/users/c0342083-f385-4e5f-9d5e-614471c42305`)
      .send({
        ...usersToCreate.user1,
        id: 'c0342083-f385-4e5f-9d5e-614471c42305',
      })
      .expect({
        statusCode: 409,
        message: 'This email already exists, are you just trying to login?',
      });
  });

  it('should save the user if all the parameters are good and if ID + email does not already exist ', async () => {
    const agent = request(app.getHttpServer());

    await agent
      .put(`/users/${usersToCreate.user1.id}`)
      .send(usersToCreate.user1)
      .expect(200)
      .expect(usersToCreate.user1);

    await agent
      .post('/auth/token')
      .send({
        email: usersToCreate.user1.email,
        password: usersToCreate.user1.password,
      })
      .expect(201);
  });
});
