import {
  Entity,
  PrimaryColumn,
  Column,
  ManyToOne,
} from 'typeorm';
import {
  IsString,
  IsInt,
  Length,
  IsUUID,
  IsEmail,
  IsNotEmpty,
} from 'class-validator';
import { User } from '../users/user.entity';

@Entity()
export class Email {
  @PrimaryColumn() id: string;

  @Column() subject: string;

  @ManyToOne(type => User, user => user)
  fromUser: User;

  @Column() toEmail: string;

  @Column('text') body: string;
}

export class SendEmailDto {
  @IsUUID('4')
  @IsNotEmpty()
  id: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 100)
  subject: string;

  @IsEmail()
  @IsNotEmpty()
  toEmail: string;

  @IsString()
  @IsNotEmpty()
  @Length(6, 5000)
  body: string;
}
