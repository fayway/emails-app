import request from 'supertest';
import { Test } from '@nestjs/testing';
import { AppModule } from './../src/app.module';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { BaseTestingModule } from './base-testing.module';
import { TestUtilsService } from './test.utils.service';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let testUtilsService: TestUtilsService;

  beforeEach(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [BaseTestingModule, AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    testUtilsService = moduleFixture.get(TestUtilsService);

    // clean the database
    await testUtilsService.cleanAllDatabaseEntities();
  });

  afterEach(async () => {
    // close the database connection
    // this is important otherwise not the first test but others
    // won't be able to connect and will timeout without other error...
    await testUtilsService.closeDbConnection();
  });

  it('/GET /', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });
});
