import { ConnectionOptions } from 'typeorm';

interface BaseConnectionOptions {
  type: 'mysql';
  host: string;
  entities: string[];
  // if true, remove all the data when the app starts
  synchronize: boolean;
}

const base: BaseConnectionOptions = {
  type: 'mysql',
  host: 'localhost',
  entities: ['src/**/**.entity{.ts,.js}'],
  // if true, remove all the data when the app starts
  synchronize: false,
};

// @todo update username, password and database
export const prod: ConnectionOptions = {
  ...base,
  port: 3312,
  username: 'root',
  password: 'dev-pw',
  database: 'prod-emails-app',
  // if true, remove all the data when the app starts
  synchronize: false,
};

export const staging: ConnectionOptions = {
  ...base,
  port: 3312,
  username: 'root',
  password: 'dev-pw',
  database: 'staging-emails-app',
  // if true, remove all the data when the app starts
  synchronize: false,
};

export const dev: ConnectionOptions = {
  ...base,
  port: 3312,
  username: 'root',
  password: 'dev-pw',
  database: 'dev-emails-app',
  // if true, remove all the data when the app starts
  synchronize: false,
};

export const test: ConnectionOptions = {
  ...base,
  port: 3312,
  username: 'root',
  password: 'dev-pw',
  database: 'test-emails-app',
  // if true, remove all the data when the app starts
  synchronize: true,
};
