import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { EmailsModule } from './emails/emails.module';

export function loadEmailsModule() {
  return EmailsModule;
}

const routes: Route[] = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: 'emails', loadChildren: loadEmailsModule },
      {
        path: '**',
        redirectTo: 'emails/inbox',
      },
    ],
  },
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [MainComponent],
})
export class MainModule {}
