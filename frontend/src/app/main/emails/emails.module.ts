import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailsComponent } from './emails.component';
import { EmailComponent } from './email/email.component';
import { PageInboxComponent } from './page-inbox/page-inbox.component';
import { PageDraftsComponent } from './page-drafts/page-drafts.component';
import { PageSentComponent } from './page-sent/page-sent.component';
import { RouterModule, Route } from '@angular/router';
import { NewEmailComponent } from './new-email/new-email.component';

const routes: Route[] = [
  { path: 'inbox', component: PageInboxComponent },
  { path: 'sent', component: PageSentComponent },
  { path: 'drafts', component: PageDraftsComponent },
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [
    EmailsComponent,
    EmailComponent,
    PageInboxComponent,
    PageDraftsComponent,
    PageSentComponent,
    NewEmailComponent,
  ],
  entryComponents: [NewEmailComponent],
})
export class EmailsModule {}
