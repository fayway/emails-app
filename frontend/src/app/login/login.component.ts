import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { catchError, tap } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

enum FormStatus {
  enable = 'ENABLE',
  disable = 'DISABLE',
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  blockLogIn = false;

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private router: Router,
    private authService: AuthService,
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.minLength(3)]],
      password: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

  logIn(): void {
    const { email, password } = this.loginForm.value;
    this.enableOrDisableForm(FormStatus.disable);

    this.authService
      .logIn(email, password)
      .pipe(
        tap(() => {
          const snackBar = this.snackBar.open(
            'Login successful (redirecting...)',
            undefined,
            { duration: 3000 },
          );

          snackBar
            .afterDismissed()
            .pipe(tap(() => this.router.navigate(['/app'])))
            .subscribe();
        }),
        catchError(e => {
          this.enableOrDisableForm(FormStatus.enable);

          this.snackBar.open(e.error.message, undefined, { duration: 3000 });
          return EMPTY;
        }),
      )
      .subscribe();
  }

  private enableOrDisableForm(formStatus: FormStatus): void {
    this.blockLogIn = formStatus === FormStatus.disable ? true : false;

    const formMethod: 'disable' | 'enable' =
      formStatus === FormStatus.disable ? 'disable' : 'enable';

    Object.keys(this.loginForm.controls).forEach(controlName =>
      this.loginForm.controls[controlName][formMethod](),
    );
  }
}
