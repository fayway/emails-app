import {
  createParamDecorator,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Entity } from '../../entities/entity';

export const checkSameIdUrlBody = <T extends Entity>(
  id: string,
  resource: T
) => {
  if (id !== resource.id) {
    throw new HttpException(
      'ID in the URL and the ID into the body must be the same',
      HttpStatus.BAD_REQUEST
    );
  }
};
