import { Component, Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as Path from 'path';
import { DatabaseService } from '../src/database/database.service';
import { BDD_ENTITIES } from '../src/entities/entity';
import { Repository } from 'typeorm';

@Injectable()
export class TestUtilsService {
  constructor(private databaseService: DatabaseService) {
    if (process.env.NODE_ENV !== 'test') {
      throw new Error(
        'This service is only to help with tests and should not be used for the main app'
      );
    }
  }

  async closeDbConnection() {
    const connection = await this.databaseService.connection;
    if (connection.isConnected) {
      await (await this.databaseService.connection).close();
    }
  }

  async cleanAllDatabaseEntities(): Promise<void> {
    const tablesToCleanPromises: Promise<Repository<any>>[] = BDD_ENTITIES.map(
      bddEntity => this.databaseService.getRepository(bddEntity)
    );

    const tablesToClean: Repository<any>[] = await Promise.all(
      tablesToCleanPromises
    );

    await Promise.all(
      tablesToClean.map(tableToClean => tableToClean.clear().catch())
    );
  }
}
